package me.weathersboyle.hackernewsclient.data.hackernews

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import me.weathersboyle.hackernewsclient.data.hackernews.entities.CommentIds
import me.weathersboyle.hackernewsclient.data.hackernews.entities.HackerNewsItem
import me.weathersboyle.hackernewsclient.data.hackernews.entities.HackerNewsItemDao

class Converters {
    val gson = Gson()

    @TypeConverter
    fun deserializeCommentIds(json: String?): CommentIds {
        if (json == null) return emptyList()

        val type = object : TypeToken<CommentIds>() {}.type
        return gson.fromJson(json, type) ?: emptyList()
    }

    @TypeConverter
    fun serializeCommentIds(commentIds: CommentIds?): String? {
        return gson.toJson(commentIds)
    }
}

@Database(entities = [HackerNewsItem::class], version = 1)
@TypeConverters(Converters::class)
abstract class HackerNewsDatabase : RoomDatabase() {

    abstract fun newsItemDao(): HackerNewsItemDao
}
