package me.weathersboyle.hackernewsclient.data.hackernews.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

typealias CommentIds = List<Long>

@Entity(tableName = "news_table")
data class HackerNewsItem(
    @PrimaryKey
    @field:Json(name = "id")
    val id: Long,
    @field:Json(name = "title")
    val title: String?,
    @field:Json(name = "by")
    val author: String?,
    @field:Json(name = "parent")
    val parent: Long?,
    @field:Json(name = "kids")
    val comments: CommentIds,
    @field:Json(name = "type")
    val type: String,
    @field:Json(name = "text")
    val text: String?,

    var relevance: Int
)
