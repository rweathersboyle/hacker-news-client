package me.weathersboyle.hackernewsclient.data.hackernews.entities

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Transaction

// TODO it may make more sense for queries against news table to be more abstract and move some business logic into repo/VM

@Dao
abstract class HackerNewsItemDao {

    @Query("SELECT * FROM news_table WHERE relevance > 0 AND type = 'story' ORDER BY relevance ASC")
    abstract fun getTopStories(): LiveData<List<HackerNewsItem>>

    @Query("SELECT * FROM news_table WHERE id = :itemId")
    abstract fun getNewsItem(itemId: Long): LiveData<HackerNewsItem>

    @Insert(onConflict = REPLACE)
    abstract fun addNewsItems(vararg newsItems: HackerNewsItem)

    @Query("UPDATE news_table SET relevance = 0")
    abstract fun resetRelevance()

    @Query("SELECT * FROM news_table WHERE type = 'comment' AND parent = :itemId")
    abstract fun getNewsItemComments(itemId: Long): LiveData<List<HackerNewsItem>>

    @Transaction
    open fun addTopStories(vararg newsItems: HackerNewsItem) {
        resetRelevance()
        addNewsItems(*newsItems)
    }
}
