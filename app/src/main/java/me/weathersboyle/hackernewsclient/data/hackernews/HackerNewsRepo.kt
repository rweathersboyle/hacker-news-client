package me.weathersboyle.hackernewsclient.data.hackernews

import androidx.lifecycle.LiveData
import me.weathersboyle.hackernewsclient.data.hackernews.entities.HackerNewsItem

interface HackerNewsRepo {

    val topStories: LiveData<List<HackerNewsItem>>

    fun getNewsItem(itemId: Long): LiveData<HackerNewsItem>

    fun getStoryComments(storyId: Long): LiveData<List<HackerNewsItem>>

    suspend fun refreshTopStories()

    suspend fun refreshStoryComments(story: HackerNewsItem)
}
