package me.weathersboyle.hackernewsclient.data.hackernews

import kotlinx.coroutines.Deferred
import me.weathersboyle.hackernewsclient.data.hackernews.entities.HackerNewsItem
import retrofit2.http.GET
import retrofit2.http.Path

interface HackerNewsApi {

    @GET("topstories.json")
    fun getTopStories(): Deferred<List<Long>>

    @GET("item/{itemId}.json")
    fun getItem(@Path("itemId") itemId: Long): Deferred<HackerNewsItem>
}
