package me.weathersboyle.hackernewsclient.data.hackernews

import androidx.lifecycle.LiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.withContext
import me.weathersboyle.hackernewsclient.data.hackernews.entities.HackerNewsItem
import me.weathersboyle.hackernewsclient.data.hackernews.entities.HackerNewsItemDao
import kotlin.math.min

class HackerNewsRepoImpl(
    private val hackerNewsApi: HackerNewsApi,
    hackerNewsDatabase: HackerNewsDatabase
) : HackerNewsRepo {

    private val newsItemDao: HackerNewsItemDao = hackerNewsDatabase.newsItemDao()

    override val topStories: LiveData<List<HackerNewsItem>> = newsItemDao.getTopStories()

    override fun getNewsItem(itemId: Long): LiveData<HackerNewsItem> = newsItemDao.getNewsItem(itemId)

    override fun getStoryComments(storyId: Long): LiveData<List<HackerNewsItem>> {
        return newsItemDao.getNewsItemComments(storyId)
    }

    override suspend fun refreshTopStories() {
        withContext(Dispatchers.IO) {
            val topStories = hackerNewsApi.getTopStories().await()
            topStories.subList(0, 20)
                .mapIndexed { index, newsItem ->
                    async {
                        hackerNewsApi.getItem(newsItem).await().apply {
                            relevance = index + 1
                        }
                    }
                }
                .awaitAll()
                .also {
                    newsItemDao.addTopStories(*it.toTypedArray())
                }
        }
    }

    override suspend fun refreshStoryComments(story: HackerNewsItem) {
        withContext(Dispatchers.IO) {
            val comments = story.comments
            comments.subList(0, min(comments.size, 25))
                .map {
                    async {
                        hackerNewsApi.getItem(it).await()
                    }
                }
                .awaitAll()
                .also {
                    newsItemDao.addNewsItems(*it.toTypedArray())
                }
        }
    }
}
