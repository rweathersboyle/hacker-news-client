package me.weathersboyle.hackernewsclient.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import me.weathersboyle.hackernewsclient.R
import me.weathersboyle.hackernewsclient.data.hackernews.entities.HackerNewsItem
import me.weathersboyle.hackernewsclient.ui.topstories.TopStoriesFragment
import me.weathersboyle.hackernewsclient.ui.story.StoryFragment

class MainActivity : AppCompatActivity(), Navigator {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigateToTopStories()
    }

    override fun navigateToTopStories() {
        showFragment {
            TopStoriesFragment.newInstance()
        }
    }

    override fun navigateToStory(story: HackerNewsItem) {
        showFragment(true) {
            StoryFragment.newInstance(story.id)
        }
    }

    private fun showFragment(addToBackStack: Boolean = false, viewCreator: () -> Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
            .replace(
                R.id.container,
                viewCreator()
            )
        if (addToBackStack) {
            transaction.addToBackStack(null)
        }
        transaction.commit()
    }
}
