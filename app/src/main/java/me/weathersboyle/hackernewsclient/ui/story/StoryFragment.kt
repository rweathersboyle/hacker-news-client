package me.weathersboyle.hackernewsclient.ui.story

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import me.weathersboyle.hackernewsclient.R
import me.weathersboyle.hackernewsclient.data.hackernews.entities.HackerNewsItem
import me.weathersboyle.hackernewsclient.ui.BaseFragment
import me.weathersboyle.hackernewsclient.ui.NewsItemListAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class StoryFragment : BaseFragment() {

    companion object {
        private const val ARG_STORY_ID = "storyId"

        fun newInstance(itemId: Long) = StoryFragment().apply {
            arguments = Bundle().apply {
                putLong(ARG_STORY_ID, itemId)
            }
        }
    }

    override val layoutResId: Int = R.layout.story_fragment

    @BindView(R.id.title)
    lateinit var title: TextView
    @BindView(R.id.author)
    lateinit var author: TextView
    @BindView(R.id.commentsRecyclerView)
    lateinit var commentsRecyclerView: RecyclerView

    private lateinit var commentsAdapter: NewsItemListAdapter

    private val viewModel: StoryViewModel by viewModel {
        // TODO handle invalid id at some point
        val storyId = arguments?.getLong(ARG_STORY_ID) ?: -1
        parametersOf(storyId)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        commentsAdapter = NewsItemListAdapter {
            // TODO onClick comment
        }
        commentsRecyclerView.adapter = commentsAdapter
        commentsRecyclerView.layoutManager = LinearLayoutManager(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.story.observe(
            this,
            Observer<HackerNewsItem> {
                title.text = it.title
                author.text = it.author
            }
        )
        viewModel.comments.observe(
            this,
            Observer<List<HackerNewsItem>> { commentsAdapter.submitList(it) }
        )
    }
}
