package me.weathersboyle.hackernewsclient.ui.story

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import kotlinx.coroutines.launch
import me.weathersboyle.hackernewsclient.data.hackernews.HackerNewsRepo
import me.weathersboyle.hackernewsclient.data.hackernews.entities.HackerNewsItem
import me.weathersboyle.hackernewsclient.ui.BaseViewModel

class StoryViewModel(
    storyId: Long,
    private val hackerNewsRepo: HackerNewsRepo
) : BaseViewModel() {

    private var commentsRefreshed = false

    val story: LiveData<HackerNewsItem> = Transformations.map(hackerNewsRepo.getNewsItem(storyId)) {
        if (!commentsRefreshed) {
            refreshComments(it)
        }

        it
    }
    val comments: LiveData<List<HackerNewsItem>> = hackerNewsRepo.getStoryComments(storyId)

    private fun refreshComments(story: HackerNewsItem) {
        commentsRefreshed = true

        // TODO error handling
        viewModelScope.launch {
            hackerNewsRepo.refreshStoryComments(story)
        }
    }
}
