package me.weathersboyle.hackernewsclient.ui.topstories

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import me.weathersboyle.hackernewsclient.R
import me.weathersboyle.hackernewsclient.data.hackernews.entities.HackerNewsItem
import me.weathersboyle.hackernewsclient.ui.BaseFragment
import me.weathersboyle.hackernewsclient.ui.Event
import me.weathersboyle.hackernewsclient.ui.Navigator
import me.weathersboyle.hackernewsclient.ui.NewsItemListAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class TopStoriesFragment : BaseFragment() {

    companion object {
        fun newInstance() = TopStoriesFragment()
    }

    override val layoutResId: Int = R.layout.top_stories_fragment

    @BindView(R.id.topStoriesRecyclerView)
    lateinit var topStoriesRecyclerView: RecyclerView

    private val viewModel: TopStoriesViewModel by viewModel()

    private var navigator: Navigator? = null

    private lateinit var topStoriesAdapter: NewsItemListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        topStoriesAdapter = NewsItemListAdapter(viewModel::onStoryClick)
        topStoriesRecyclerView.adapter = topStoriesAdapter
        topStoriesRecyclerView.layoutManager = LinearLayoutManager(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.topStories.observe(
            this,
            Observer<List<HackerNewsItem>> { topStoriesAdapter.submitList(it) }
        )
        viewModel.navigateToStoryEvents.observe(
            this,
            Observer<Event<HackerNewsItem>> {
                it.getData()?.run {
                    navigateToStory(this)
                }
            }
        )
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        navigator = context as? Navigator
    }

    override fun onResume() {
        super.onResume()

        viewModel.onViewResume()
    }

    private fun navigateToStory(story: HackerNewsItem) {
        navigator?.run {
            navigateToStory(story)
        }
    }
}
