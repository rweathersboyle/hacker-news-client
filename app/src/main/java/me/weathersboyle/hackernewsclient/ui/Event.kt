package me.weathersboyle.hackernewsclient.ui

open class Event<out T>(private val data: T) {

    var read = false
        private set

    fun getData(): T? = if (read) null else data.also { read = true }
}
