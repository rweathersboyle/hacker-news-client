package me.weathersboyle.hackernewsclient.ui

import me.weathersboyle.hackernewsclient.data.hackernews.entities.HackerNewsItem

interface Navigator {

    fun navigateToTopStories()

    fun navigateToStory(story: HackerNewsItem)
}
