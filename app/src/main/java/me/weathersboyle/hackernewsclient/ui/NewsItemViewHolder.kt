package me.weathersboyle.hackernewsclient.ui

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import me.weathersboyle.hackernewsclient.R
import me.weathersboyle.hackernewsclient.data.hackernews.entities.HackerNewsItem

class NewsItemViewHolder(
    itemView: View,
    private val onItemClickListener: (HackerNewsItem) -> Unit
) : RecyclerView.ViewHolder(itemView) {

    init {
        ButterKnife.bind(this, itemView)
    }

    @BindView(R.id.title)
    lateinit var title: TextView

    fun bind(item: HackerNewsItem) {
        title.text = when (item.type) {
            "story" -> item.title
            "comment" -> item.text
            else -> null
        } ?: ""

        itemView.setOnClickListener { onItemClickListener(item) }
    }
}
