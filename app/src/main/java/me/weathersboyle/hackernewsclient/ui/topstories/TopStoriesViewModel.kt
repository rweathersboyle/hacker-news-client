package me.weathersboyle.hackernewsclient.ui.topstories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.launch
import me.weathersboyle.hackernewsclient.data.hackernews.HackerNewsRepo
import me.weathersboyle.hackernewsclient.data.hackernews.entities.HackerNewsItem
import me.weathersboyle.hackernewsclient.ui.BaseViewModel
import me.weathersboyle.hackernewsclient.ui.Event

class TopStoriesViewModel(private val hackerNewsRepo: HackerNewsRepo) : BaseViewModel() {

    private val _navigateToStoryEvents: MutableLiveData<Event<HackerNewsItem>> = MutableLiveData()

    val topStories: LiveData<List<HackerNewsItem>> = hackerNewsRepo.topStories
    val navigateToStoryEvents: LiveData<Event<HackerNewsItem>>
        get() = _navigateToStoryEvents

    fun onViewResume() {
        refreshTopStories()
    }

    fun onStoryClick(story: HackerNewsItem) {
        _navigateToStoryEvents.value = Event(story)
    }

    private fun refreshTopStories() {
        // TODO error handling
        viewModelScope.launch {
            hackerNewsRepo.refreshTopStories()
        }
    }
}
