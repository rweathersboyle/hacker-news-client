package me.weathersboyle.hackernewsclient.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import me.weathersboyle.hackernewsclient.R
import me.weathersboyle.hackernewsclient.data.hackernews.entities.HackerNewsItem

class DiffCallback : DiffUtil.ItemCallback<HackerNewsItem>() {

    override fun areItemsTheSame(oldItem: HackerNewsItem, newItem: HackerNewsItem): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: HackerNewsItem, newItem: HackerNewsItem): Boolean {
        // TODO assumes content of a given article is never updated
        return true
    }
}

class NewsItemListAdapter(private val onItemClickListener: (HackerNewsItem) -> Unit) :
    ListAdapter<HackerNewsItem, NewsItemViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        NewsItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.news_list_item,
                parent,
                false
            ),
            onItemClickListener
        )

    override fun onBindViewHolder(holder: NewsItemViewHolder, position: Int) = holder.bind(getItem(position))
}
