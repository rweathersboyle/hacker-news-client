package me.weathersboyle.hackernewsclient

import android.app.Application
import androidx.room.Room
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import me.weathersboyle.hackernewsclient.data.hackernews.HackerNewsApi
import me.weathersboyle.hackernewsclient.data.hackernews.HackerNewsDatabase
import me.weathersboyle.hackernewsclient.data.hackernews.HackerNewsRepo
import me.weathersboyle.hackernewsclient.data.hackernews.HackerNewsRepoImpl
import me.weathersboyle.hackernewsclient.ui.story.StoryViewModel
import me.weathersboyle.hackernewsclient.ui.topstories.TopStoriesViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

const val HACKER_NEWS_BASE_URL = "https://hacker-news.firebaseio.com/v0/"

class HackerNewsClientApplication : Application() {

    val appModule = module {
        single {
            OkHttpClient.Builder()
                .apply {
                    if (BuildConfig.DEBUG) {
                        addInterceptor(
                            HttpLoggingInterceptor().apply {
                                level = HttpLoggingInterceptor.Level.BODY
                            }
                        )
                    }

                    connectTimeout(30, TimeUnit.SECONDS)
                }
                .build()
        }

        single<Converter.Factory> {
            MoshiConverterFactory.create()
        }

        single<Retrofit> {
            Retrofit.Builder()
                .baseUrl(HACKER_NEWS_BASE_URL)
                .client(get())
                .addConverterFactory(get())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
        }

        single<HackerNewsApi> {
            get<Retrofit>().create(HackerNewsApi::class.java)
        }

        single {
            Room.databaseBuilder(
                androidContext(),
                HackerNewsDatabase::class.java,
                "news_database"
            ).build()
        }

        single<HackerNewsRepo> {
            HackerNewsRepoImpl(get(), get())
        }

        viewModel {
            TopStoriesViewModel(get())
        }

        viewModel { (itemId: Long) ->
            StoryViewModel(itemId, get())
        }
    }

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@HackerNewsClientApplication)
            modules(appModule)
        }
    }
}
